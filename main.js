const http = require('http'); 
const querystring = require('querystring');
const port = 3000;
const server = http.createServer();
server.listen(port);

server.on('request', (req, res) => {
	let data = '';
	req.on('data', chunk => data += chunk);
	req.on('end', () => {
		let name = req.url.replace('/', '').split(':');
		let hash = '';
		createHash(name.join(''))
			.then((hash) => {
				res.writeHead(200, 'OK');
				data = JSON.stringify ({
					'firstName': name[0],
					'lastName': name[1],
					'secretKey': hash
				});
				res.write(data);
				res.end();
			})
			.catch(err => console.log(err));
	});
});

function createHash(name) {
	return new Promise((resolve, reject) => {
		let options = { 
		hostname: 'netology.tomilomark.ru',
		path:'/api/v1/hash', 
		method:'POST', 
		headers: { 
			'Content-Type':'application/json', 
		} 
	}
 
	let data = querystring.stringify(name);
	let request = http.request(options); 

	request.write(data); 
	request.on('error', err => console.error(err));
	request.on('response', response => { 
		let data = ''; 
		response.on('data', function (chunk) {  
			data += chunk; 
		}); 
		response.on('end', function () { 
			hash = JSON.parse(data).hash;
			resolve(hash);
		}); 
	}); 

	request.end();
	});
} 